import log from "@ajar/marker";


/**
 * Query string challenge
 * 
 * A valid URL (for the sake of this exercise) is:
 * The URL must start with either http or https and
 * then followed by :// and
 * then it must contain www. and
 * then followed by subdomain of length (2, 256) and
 * last part contains top level domain like .com, .org etc.
 * 
 * Read about query strings here: https://en.wikipedia.org/wiki/Query_string
 * 
 * In this exercise you should create a function queryProcessor which:
 * 1. will recieve url of type string
 * 2. will return either error object or query object:
 *    2.1. if url is not valid, return an error object: {error: 'URL is not valid'}
 *    2.2. else return a query object which contains keys:value:
 *          For example:
 *              Input: https://example.com/over/there?name=Tal&age=30&mood=depressed
 *              Output: {name: Tal, age: 30, mood: depressed}
 * 
 * 
 * You should use regex for validation purposes
*/

/**
 * Solution
 */
interface IURLError {
    error: string;
}
interface IQueryParameters {
    [key: string]: any
}


function queryProcessor(url: string): IQueryParameters | IURLError {
    // Googled it
    const urlValidator = /[(http(s)?):\/\/(www\.)a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;
    if (!urlValidator.test(url)) {
        return {
            error: "URL is not valid"
        }
    }
    const queryParameters = /\?(.*)$/;
    const parameters = queryParameters.exec(url);
    if (parameters === undefined || parameters === null || parameters.length < 1) {
        return {
            error: "URL does not contains any variables"
        };
    }
    return parameters[1].split('&').reduce((acc, curr) => {
        const [key,value] = curr.split("=");
        acc[key] = value;
        return acc;
    }, {} as IQueryParameters);
}

// Expecting { name: 'Tal', age: '30', mood: 'depressed' }
console.log(queryProcessor('https://www.example.com/over/there?name=Tal&age=30&mood=depressed'))
// Expecting { error: 'URL is not valid'}
console.log(queryProcessor('HiImNotAURL'));
// Expecting { error: 'URL does not contains any variables'}
console.log(queryProcessor('http://www.google.com'));









